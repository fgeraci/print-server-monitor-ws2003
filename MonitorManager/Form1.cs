﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Utilities;

namespace MonitorManager
{
    public partial class Form1 : Form
    {

        private bool spooler = false;
        private SpoolerReader spoolerReader;

        public Form1()
        {
            InitializeComponent();
            this.spoolerReader = new SpoolerReader(this.logTextBox);
            this.spoolerStatusLabel.Text = "Spooler Monitor Off";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void spoolerButton_Click(object sender, EventArgs e)
        {
            if (!this.spooler)
            {
                this.spooler = !this.spooler;
                try
                {
                    this.spoolerReader.Start();
                    this.spoolerStatusLabel.ForeColor = Color.Green;
                    this.spoolerStatusLabel.Text = "Spooler Monitor On";
                }
                catch (Exception exception)
                {
                    this.logTextBox.Text += exception.Message + "\n";
                }
            }
            else
            {
                this.spooler = !this.spooler;
                try
                {
                    this.spoolerReader.Stop();
                    this.spoolerStatusLabel.ForeColor = Color.Red;
                    this.spoolerStatusLabel.Text = "Spooler Monitor Off";
                }
                catch (Exception exception)
                {
                    this.logTextBox.Text += exception.Message + "\n";
                }
            }
        }
    }
}
