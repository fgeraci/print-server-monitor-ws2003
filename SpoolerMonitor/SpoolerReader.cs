﻿using System;
using System.IO; // for File and FileInfo
using System.Text;
using System.Threading; // for Thread
using System.Diagnostics; // for Process class
using System.Collections.Generic;
using System.Collections;
using System.Windows.Forms;

namespace Utilities
{

    public class SpoolerReader
    {
        private bool run;
        private string spoolerPath;
        private string batchPath;
        private string batchFile = "KickSpooler.bat";
        private int spool = 90;
        private Process process;
        Thread thread;
        private TextBox logger;

        public SpoolerReader(TextBox logger)
        {
            this.logger = logger;
            this.spoolerPath = Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\spool\PRINTERS";
            this.batchPath = Environment.GetFolderPath(Environment.SpecialFolder.System) + this.batchFile + @"\";
            // this.spoolerPath = @"C:\Users\Fernando\Documents\Test"; FOR DEBUGGING
            this.thread = new Thread(this.read);
        }

        public void Start()
        {
            if (!this.thread.IsAlive)
            {
                this.run = true;
                thread.Start();
            }
            else
            {
                this.read();
            }
            this.Log("Spooler Monitor Started");
        }

        public void Stop()
        {
            this.run = false;
            this.Log("Spooler Monitor Stopped");
        }

        private void read()
        {

            while (this.run)
            {

                try
                {
                    string[] files = Directory.GetFiles(this.spoolerPath);
                    Hashtable sizes = this.getSizes(files);
                    Thread.Sleep(this.spool * 1000);
                    // Console.WriteLine("SYSTEM: Matching documents");
                    string[] currentFiles = Directory.GetFiles(this.spoolerPath);
                    int index = 0;
                    foreach (string file in currentFiles)
                    {
                        if (sizes.ContainsKey(file))
                        {
                            // Console.WriteLine("SYSTEM: File {0} with size {1}", file, sizes[file]);
                            if (this.matchSize(file, (long)sizes[file]))
                            {
                                // Approach A - Call a batch file to delete all the files and kick the spool
                                this.callBatchFile();
                                this.Log("Spooler cleaned");
                                break;
                            }
                        }
                        ++index;
                    }
                }
                catch (Exception e)
                {
                    this.run = false;
                    this.Log(e.Message);
                }
            }
        }

        private bool isContained(string file, string[] files)
        {
            foreach (string f in files)
            {
                if (file == f) return true;
            }
            return false;
        }

        private void Log(string message)
        {
            this.logger.Text += message + Environment.NewLine;
        }

        private Hashtable getSizes(string[] files)
        {
            Hashtable sizes = new Hashtable();
            for (int i = 0; i < files.Length; ++i)
            {
                FileInfo file = new FileInfo(files[i]);
                sizes.
                    Add(files[i], file.Length);
            }
            return sizes;
        }

        private bool matchSize(string file, long originalSize)
        {
            FileInfo f = new FileInfo(file);
            return f.Length == originalSize;
        }

        private void callBatchFile()
        {
            this.process = new Process();
            this.process.StartInfo.WorkingDirectory = @"C:\Scripts";
            this.process.StartInfo.FileName = this.batchFile;
            this.process.StartInfo.CreateNoWindow = true;
            this.process.Start();
        }

    }
}

